﻿#include "Logger.h"

#include "spdlog/sinks/stdout_color_sinks.h"

namespace JEngine
{
	std::shared_ptr<spdlog::logger> Logger::m_sCoreLogger;
	std::shared_ptr<spdlog::logger> Logger::m_sClientLogger;

	void Logger::Init()
	{
		spdlog::set_pattern("%^[%T] %n: %v%$");
		
		m_sCoreLogger = spdlog::stdout_color_mt("Core");
		m_sCoreLogger->set_level(spdlog::level::trace);
		
		m_sClientLogger = spdlog::stderr_color_mt("Client");
		m_sCoreLogger->set_level(spdlog::level::trace);
	}
}
