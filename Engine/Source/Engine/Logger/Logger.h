#pragma once

#include "Core/Core.h"
#include "spdlog/spdlog.h"

namespace JEngine
{
	class ENGINE_API Logger
	{
	public:
		static void Init();

		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return m_sCoreLogger; }
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return m_sClientLogger; }

	private:
		static std::shared_ptr<spdlog::logger> m_sCoreLogger;
		static std::shared_ptr<spdlog::logger> m_sClientLogger;
	};
}
