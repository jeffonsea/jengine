﻿#pragma once

#include "App.h"

#ifdef PLATFORM_WINDOWS

namespace JEngine
{
	/**
	 * \brief Creates an instance of the application type for the project.
	 * \return Shared pointer to the application type.
	 */
	extern std::shared_ptr<App> CreateApplication();
}

int main(int argc, char** argv)
{
	const std::shared_ptr<JEngine::App>& app = JEngine::CreateApplication();
	app->Run();
	    
	return 0;
}

#endif