﻿#pragma once

#include <memory>
#include "Core.h"

namespace JEngine
{
	/**
	 * \brief Application base class.
	 */
	class ENGINE_API App
	{
	public:
		App();
		virtual ~App();
		void Run();
		virtual const char* GetName() const;
	};

	// To be defined on game.
	std::shared_ptr<App> CreateApplication();
}
