#include <JEngine.h>
#include <iostream>

class Game : public JEngine::App
{
public:
    Game() = default;
    virtual ~Game() override = default;
    const char* GetName() const override { return "Game"; }
};

std::shared_ptr<JEngine::App> JEngine::CreateApplication()
{
    const std::shared_ptr<App>& application = std::make_shared<Game>();

    std::cout << "Created an application instance for " << application->GetName() << std::endl;

    return application;
}
