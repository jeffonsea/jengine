#include <JEngine.h>
#include <iostream>

class Editor : public JEngine::App
{
public:
    Editor() = default;
    virtual ~Editor() override = default;
    const char* GetName() const override { return "Editor"; }
};

std::shared_ptr<JEngine::App> JEngine::CreateApplication()
{
    const std::shared_ptr<App>& application = std::make_shared<Editor>();

    std::cout << "Created an application instance for " << application->GetName() << std::endl;

    return application;
}